/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import DAO.DAO_Neo4jGraphDB;

/**
 *
 * @author Lech Jankowski
 */
public class ArmacTestApp {
    
    public static void main( String[] args )
    {
        DAO_Neo4jGraphDB armacTest = new DAO_Neo4jGraphDB();
        armacTest.eraseDatabase();

        /*
        armacTest.addCity("Zurich");
        armacTest.addCity("Dublin");
        armacTest.addCity("London");
        armacTest.addCity("Paris");
        armacTest.addCity("Zurich");
        armacTest.addCity("Warsaw");
        armacTest.addCity("Berlin");
        armacTest.addCity("Berlin");
        armacTest.addTransit("Dublin", "London", .45);
        armacTest.addTransit("Dublin", "Paris", 2);
        armacTest.addTransit("London", "Paris", 1.45);
        armacTest.addTransit("London", "Zurich", 4.15);
        armacTest.addTransit("Paris", "Zurich", 2);
        armacTest.addTransit("Dublin", "Warsaw", 2.5);
        armacTest.addTransit("London", "Warsaw", 2);
        armacTest.addTransit("Paris", "Warsaw", 2);
        */
        
        String filename = "/ArmacTestWebApp/excel/test.xlsx";
        armacTest.importExcelData(filename);        

        System.out.println(armacTest.getAllCities());
        System.out.println(armacTest.getShortestPath("Dublin", "Zurich"));
        System.out.println(armacTest.getPathsAscendingByTime("Dublin", "Zurich"));
        System.out.println(armacTest.getAllTransits());
    }    
    
}
