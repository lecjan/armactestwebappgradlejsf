/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package DAO;

/**
 *
 * @author Lech Jankowski
 */

import DTO.CityList;
import DTO.TransitPathWeighted;
import DTO.TransitPathWeightedList;
import de.vogella.algorithms.dijkstra.model.Edge;
import de.vogella.algorithms.dijkstra.model.Graph;
import de.vogella.algorithms.dijkstra.model.Vertex;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.neo4j.driver.v1.*;
import org.neo4j.driver.v1.exceptions.Neo4jException;

public class DAO_Neo4jGraphDB
{
    // credentials to GraphenmeDB cloud hosting armactest neo4j database
    private final String bolt = "bolt://hobby-dkpfgcgfojekgbkehobomfol.dbs.graphenedb.com:24786";
    private final String neo4juser = "armactestproduction";
    private final String neo4jpass = "b.Asl1GrNsaqFf.2bF9u0lJIHOGaC6l";
    
    /**
     * Erasing content of a database
     * 
     */    
    public void eraseDatabase()
    {
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        // first must delete all relations/edges/vertices
        session.run("START n=node(*) MATCH (n)-[r]-() DELETE r");
        // then delete all nodes/vertexes
        session.run("START n=node(*) MATCH (n) DELETE n");

        session.close();
        driver.close();        
    }

    /**
     * Adding a node type City into database
     * @param name City name
     * @return name City name added
     */    
    public String addCity(String name)
    {
        String addedcityname = null;
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        StatementResult result = session.run("MATCH (n:City {name:'"+name+"'}) RETURN n");
        if (result.hasNext())
        {
            // record found
            System.out.println("Duplicated node:"+name);
        } else {
            session.run("CREATE (n:City {name:'"+name+"'})");
            System.out.println("Adding node:"+name);
        }

        result = session.run("MATCH (n:City {name: '"+name+"'}) RETURN n.name AS name");
        while ( result.hasNext() )
        {
            Record record = result.next();
            addedcityname = record.get("name").asString();
            System.out.println("Confirmed node:"+addedcityname);
        }

        session.close();
        driver.close(); 
        return addedcityname;
    }

    /**
     * Same as addCity but needs an open connection with database
     * @param name City name
     * @param session open Driver session
     * @return name added City name
     */    
    public String addCityOpenConnection(String name, Session session)
    {
        String addedcityname = null;

        StatementResult result = session.run("MATCH (n:City {name:'"+name+"'}) RETURN n");
        if (result.hasNext())
        {
            // record found
            System.out.println("Duplicated node:"+name);            
        } else {
            session.run("CREATE (n:City {name:'"+name+"'})");
            System.out.println("Adding node:"+name);            
        }

        result = session.run("MATCH (n:City {name: '"+name+"'}) RETURN n.name AS name");
        while ( result.hasNext() )
        {
            Record record = result.next();
            addedcityname = record.get("name").asString();
            System.out.println("Confirmed node:"+addedcityname);
        }
        return addedcityname;
    }    
    
    /**
     * adding a relationship type TRANSIT_TO between the Cities with attribute (time)
     * @param source city name
     * @param destination city name
     * @param time transit time
     */    
    public void addTransit(String source, String destination, double time)
    {
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();
        
        StatementResult result = session.run("MATCH (n1:City {name:'"+source+"'}) - [r:TRANSIT_TO] -> (n2:City {name:'"+destination+"'}) return n1,n2,r");
        if (result.hasNext())
        {
            // record found
            System.out.println("Duplicated edge:"+source+"-"+destination);
        } else {
            session.run("MATCH (n1: City {name:'" + source + "'}),(n2: City {name:'" + destination + "'}) CREATE (n1) -[r: TRANSIT_TO {time:" + time + "}]-> (n2) RETURN n1,r,n2");
            System.out.println("Adding edge:"+source+"-"+time+"-"+destination); 

        }        
                
        result = session.run("MATCH (n1:City {name: '"+source+"'}) - [r:TRANSIT_TO {time:"+time+"}]-> (n2:City {name: '"+destination+"'}) RETURN n1.name AS n1source, n2.name AS n2destination, r.time AS rtime");
        while ( result.hasNext() )
        {
            Record record = result.next();
            System.out.println("Condirmed edge:"+record.get("n1source").asString()+", "+record.get("n2destination").asString()+", "+record.get("rtime").asDouble());
        }        
        
        session.close();
        driver.close(); 
    }    

    /**
     * adding a relationship type TRANSIT_TO between the Cities with attribute (time)
     * Connection with database needed to be open
     * @param source city name
     * @param destination city name
     * @param time transit time
     */    
    public void addTransitOpenConnection(String source, String destination, double time, Session session)
    {
        StatementResult result = session.run("MATCH (n1:City {name:'"+source+"'}) - [r:TRANSIT_TO] -> (n2:City {name:'"+destination+"'}) return n1,n2,r");
        if (result.hasNext())
        {
            // record found
            System.out.println("Duplicated edge:"+source+"-"+destination);            
        } else {
            session.run("MATCH (n1: City {name:'" + source + "'}),(n2: City {name:'" + destination + "'}) CREATE (n1) -[r: TRANSIT_TO {time:" + time + "}]-> (n2) RETURN n1,r,n2");
            System.out.println("Adding edge:"+source+"-"+time+"-"+destination); 
        }        
                
        result = session.run("MATCH (n1:City {name: '"+source+"'}) - [r:TRANSIT_TO {time:"+time+"}]-> (n2:City {name: '"+destination+"'}) RETURN n1.name AS n1source, n2.name AS n2destination, r.time AS rtime");
        while ( result.hasNext() )
        {
            Record record = result.next();
            System.out.println("Confirmed edge:"+record.get("n1source").asString()+", "+record.get("n2destination").asString()+", "+record.get("rtime").asDouble());
        }        
    }    

    /**
     * retrieving a list of all cities/nodes
     * @return CityList allCities
     */    
    public CityList getAllCities()
    {
        String addedcityname = null;
        CityList allCities = new CityList();
        
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        StatementResult result = session.run("MATCH (n:City) RETURN n.name AS name");
        while ( result.hasNext() )
        {
            Record record = result.next();
            addedcityname = record.get("name").asString();
            allCities.addCity(addedcityname);
        }

        session.close();
        driver.close(); 
        return allCities;
    }

    /**
     * retrieving a list of all transits/relationships
     * @return TransitPathWeightedList tpwl
     */    
    public TransitPathWeightedList getAllTransits()
    {
        String addedcityname = null;
        CityList allCities = new CityList();
        
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        // searching only for all 1st level connections between the node and any other
        StatementResult result = session.run("MATCH p = (n) - [r*1..1] -> () RETURN EXTRACT(n IN nodes(p)| n.name) AS cities, EXTRACT(r IN relationships(p)| r.time) AS transittimes");
        
        TransitPathWeightedList tpwl = new TransitPathWeightedList();
        
        while ( result.hasNext() )
        {
            Record record = result.next();
            
            TransitPathWeighted tpw = new TransitPathWeighted();
            
            List<Object> list1 = record.get("cities").asList();
            List<String> strings = new ArrayList<>(list1.size());
            for (Object object : list1) {
                strings.add(object != null ? object.toString() : null);
            }
            tpw.setCities(strings);
            
            List<Object> list2 = record.get("transittimes").asList();
            List<Double> doubles = new ArrayList<>(list2.size());
            for (Object object : list2) {
                doubles.add((Double)object);
            }
            tpw.setTransittime(doubles); 
            
            tpw.setTotaltransittime(tpw.getTransittime().get(0));       
            
            tpwl.addPath(tpw);
        }

        session.close();
        driver.close(); 
        return tpwl;
    }

    /**
     * retrieving a list of all direct neighbours (1st level)
     * @param source name of source city
     * @return TransitPathWeightedList tpwl
     */    
    public TransitPathWeightedList getAllNeighbours(String source)
    {
        
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        // searching for 1st level connections for particular node
        StatementResult result = session.run("MATCH p = (n:City {name: '"+source+"'}) - [r*1..1] -> () RETURN EXTRACT(n IN nodes(p)| n.name) AS cities, EXTRACT(r IN relationships(p)| r.time) AS transittimes");

        TransitPathWeightedList tpwl = new TransitPathWeightedList();
        
        while ( result.hasNext() )
        {
           Record record = result.next();
            
            TransitPathWeighted tpw = new TransitPathWeighted();
            
            List<Object> list1 = record.get("cities").asList();
            List<String> strings = new ArrayList<>(list1.size());
            for (Object object : list1) {
                strings.add(object != null ? object.toString() : null);
            }
            tpw.setCities(strings);
            
            List<Object> list2 = record.get("transittimes").asList();
            List<Double> doubles = new ArrayList<>(list2.size());
            for (Object object : list2) {
                doubles.add((Double)object);
            }
            tpw.setTransittime(doubles); 
            
            tpw.setTotaltransittime(tpw.getTransittime().get(0));       
            
            tpwl.addPath(tpw);
        }
        
        session.close();
        driver.close(); 
        return tpwl;
    }

    /**
     * retrieving a list of transit path as shortest and quickest path
     * @param source name of source city
     * @param destination name of destination city
     * @return TransitPathWeighted tpw
     */    
    public TransitPathWeighted getShortestPath(String source, String destination)
    {
        
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        // searching for all paths between source and destination calculating total transit time and returning a list ascending by time but only first element of a list which is shortest path
        StatementResult result = session.run("MATCH p=(a:City {name:'"+source+"'})-[r*..]->(b:City {name:'"+destination+"'})\n" +
"WITH p, relationships(p) AS rcollection // just for readability, alias rcollection\n" +
"RETURN EXTRACT(n IN nodes(p)| n.name) AS cities, EXTRACT(r IN relationships(p)| r.time) AS transittimes, REDUCE(totalTime=0, x IN rcollection| totalTime + x.time) AS totalTime\n" +
"ORDER BY totalTime LIMIT 1");

        TransitPathWeighted tpw = new TransitPathWeighted();
        
        while ( result.hasNext() )
        {
            Record record = result.next();

            List<Object> list1 = record.get("cities").asList();
            List<String> strings = new ArrayList<>(list1.size());
            for (Object object : list1) {
                strings.add(object != null ? object.toString() : null);
            }
            tpw.setCities(strings);
            
            List<Object> list2 = record.get("transittimes").asList();
            List<Double> doubles = new ArrayList<>(list2.size());
            for (Object object : list2) {
                doubles.add((Double)object);
            }
            tpw.setTransittime(doubles); 
            
            tpw.setTotaltransittime(record.get("totalTime").asDouble());
        }
        
        session.close();
        driver.close(); 
        return tpw;
    }    
    
    /**
     * retrieving a list of all transit paths in ascending order depending on time
     * @param source name of source city
     * @param destination name of destination city
     * @return TransitPathWeighted tpw
     */    
    public TransitPathWeightedList getPathsAscendingByTime(String source, String destination)
    {
        
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        // searching for all paths between source and destination calculating total transit time and returning a list ascending by time
        StatementResult result = session.run("MATCH p=(a:City {name:'"+source+"'})-[r*..]->(b:City {name:'"+destination+"'})\n" +
"WITH p, relationships(p) AS rcollection // just for readability, alias rcollection\n" +
"RETURN EXTRACT(n IN nodes(p)| n.name) AS cities, EXTRACT(r IN relationships(p)| r.time) AS transittimes, REDUCE(totalTime=0, x IN rcollection| totalTime + x.time) AS totalTime\n" +
"ORDER BY totalTime");
        
        TransitPathWeightedList tpwl = new TransitPathWeightedList();
        
        while ( result.hasNext() )
        {
            Record record = result.next();
            
            TransitPathWeighted tpw = new TransitPathWeighted();

            List<Object> list1 = record.get("cities").asList();
            List<String> strings = new ArrayList<>(list1.size());
            for (Object object : list1) {
                strings.add(object != null ? object.toString() : null);
            }
            tpw.setCities(strings);
            
            
            List<Object> list2 = record.get("transittimes").asList();
            List<Double> doubles = new ArrayList<>(list2.size());
            for (Object object : list2) {
                doubles.add((Double)object);
            }
            tpw.setTransittime(doubles); 
            
            tpw.setTotaltransittime(record.get("totalTime").asDouble());
            
            tpwl.addPath(tpw);
        }
        
        session.close();
        driver.close(); 
        return tpwl;
    }    

    /**
     * retrieving all data into a graph structure object
     * @return Graph graph object
     */    
    public Graph convert2Graph() {
        List<Vertex> nodes = new ArrayList<Vertex>();
        List<Edge> edges = new ArrayList<Edge>();

        CityList cities = getAllCities();
        for (int i = 0; i < cities.getCityList().size(); i++) {
                Vertex location = new Vertex(cities.getCityList().get(i), cities.getCityList().get(i));
                nodes.add(location);
        }                

        TransitPathWeightedList transits = getAllTransits();
        for (int i = 0; i < transits.getPaths().size(); i++) {
                String source = transits.getPaths().get(i).getCities().get(0);
                String destination = transits.getPaths().get(i).getCities().get(1);
                Double time = transits.getPaths().get(i).getTransittime().get(0);
                Vertex sourcevertex = new Vertex(source,source);
                Vertex destinationvertex = new Vertex(destination,destination);

                Edge link = new Edge("Edge_"+i, sourcevertex, destinationvertex, time);
                edges.add(link);                    
        }                

        // Lets check from location Warsaw to Zurich
        Graph graph = new Graph(nodes, edges);   
        return graph;
    }
    
    /**
     * importing *.xlsx Excel file workbook data into neo4j db
     * @param filename 
     * @return String import status
     */    
    private String importXLSX(String filename)
    {
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();
    
        try {
            File fileXLSX = new File(filename);
            OPCPackage pkg = OPCPackage.open(fileXLSX);
            XSSFWorkbook wb = new XSSFWorkbook(pkg);
            XSSFSheet sheet = wb.getSheetAt(0);
            XSSFRow row;
            XSSFCell cell; 
            
            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            for(int r = 0; r < rows; r++) {
                row = sheet.getRow(r);
                if(row != null) {
                    String source; 
                    if (row.getCell(0) == null) source = ""; 
                        else source = row.getCell(0).toString(); // read 1st column as source city
                    String destination;
                    if (row.getCell(1) == null) destination = ""; 
                        else destination = row.getCell(1).toString();  // read 2nd column as destination city
                    double time; 
                    if (row.getCell(2) == null) time = 0.0;
                    else time = row.getCell(2).getNumericCellValue(); // read 3rd column as transit time                    
                    addCity(source); 
                    addCity(destination);
                    addTransit(source, destination, time); 
                }
                System.out.println();
            }
            pkg.close();
            session.close();
            driver.close();
            return "Success";
        } catch(InvalidFormatException | IOException | Neo4jException ioe) {
            ioe.printStackTrace();
            return "IO Error";
        } 
    }

    /**
     * importing *.xls 97 Excel file data into neo4j db
     * @param filename 
     * @return String import status
     */    
    private String importXLS(String filename)
    {
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        try {
            File fileXLS = new File(filename);
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileXLS));
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell; 
            
            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            for(int r = 0; r < rows; r++) {
                row = sheet.getRow(r);
                if(row != null) {
                    String source; 
                    if (row.getCell(0) == null) source = ""; 
                        else source = row.getCell(0).toString(); // read 1st column as source city
                    String destination;
                    if (row.getCell(1) == null) destination = ""; 
                        else destination = row.getCell(1).toString();  // read 2nd column as destination city
                    double time; 
                    if (row.getCell(2) == null) time = 0.0;
                    else time = row.getCell(2).getNumericCellValue(); // read 3rd column as transit time                    
                    addCity(source);
                    addCity(destination); 
                    addTransit(source, destination, time); 
                }
                System.out.println();
            }
            fs.close();
            session.close();
            driver.close();        
            return "Success";
        } catch(IOException | Neo4jException ioe) {
            ioe.printStackTrace();
            return "IO Error";
        }
    }    
    
    /**
     * importing *.xls* Excel file workbook or 97 spreadsheet data into neo4j db
     * @param filename 
     * @return String import status
     */    
    public String importExcelData(String filename) {
        String result;
        String extension = "";
        if (filename != null & filename !="" & filename.indexOf(".") != -1) {
            extension = filename.substring(filename.indexOf("."));
        }
        switch (extension) {
            case ".xls":    result = importXLS(filename);
                            break;
            case ".xlsx":   result = importXLSX(filename);
                            break;
            default:        result = "wrong filename structure!";
                            break;
        }
        return result;
    }     

    
    /**
     * importing *.xlsx Excel file workbook data into neo4j db (version with open connection add methods
     * @param filename 
     * @return String import status
     */    
    private String importXLSXOpenConnection(String filename)
    {
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();
    
        try {
            File fileXLSX = new File(filename);
            OPCPackage pkg = OPCPackage.open(fileXLSX);
            XSSFWorkbook wb = new XSSFWorkbook(pkg);
            XSSFSheet sheet = wb.getSheetAt(0);
            XSSFRow row;
            XSSFCell cell; 
            
            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            for(int r = 0; r < rows; r++) {
                row = sheet.getRow(r);
                if(row != null) {
                    String source; 
                    if (row.getCell(0) == null) source = ""; 
                        else source = row.getCell(0).toString(); // read 1st column as source city
                    String destination;
                    if (row.getCell(1) == null) destination = ""; 
                        else destination = row.getCell(1).toString();  // read 2nd column as destination city
                    double time; 
                    if (row.getCell(2) == null) time = 0.0;
                    else time = row.getCell(2).getNumericCellValue(); // read 3rd column as transit time                    
                    addCityOpenConnection(source, session);
                    addCityOpenConnection(destination, session);
                    addTransitOpenConnection(source, destination, time, session);
                }
                System.out.println();
            }
            pkg.close();
            session.close();
            driver.close();
            return "Success";
        } catch(InvalidFormatException | IOException | Neo4jException ioe) {
            ioe.printStackTrace();
            return "IO Error";
        } 
    }

    /**
     * importing *.xls 97 Excel file data into neo4j db (version with open connection to db off add methods)
     * @param filename 
     * @return String import status
     */    
    private String importXLSOpenConnection(String filename)
    {
        Driver driver = GraphDatabase.driver( bolt, AuthTokens.basic( neo4juser, neo4jpass));
        Session session = driver.session();

        try {
            File fileXLS = new File(filename);
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileXLS));
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell; 
            
            int rows; // No of rows
            rows = sheet.getPhysicalNumberOfRows();

            for(int r = 0; r < rows; r++) {
                row = sheet.getRow(r);
                if(row != null) {
                    String source; 
                    if (row.getCell(0) == null) source = ""; 
                        else source = row.getCell(0).toString(); // read 1st column as source city
                    String destination;
                    if (row.getCell(1) == null) destination = ""; 
                        else destination = row.getCell(1).toString();  // read 2nd column as destination city
                    double time; 
                    if (row.getCell(2) == null) time = 0.0;
                    else time = row.getCell(2).getNumericCellValue(); // read 3rd column as transit time
                    addCityOpenConnection(source, session); 
                    addCityOpenConnection(destination, session); 
                    addTransitOpenConnection(source, destination, time, session); 
                }
                System.out.println();
            }
            fs.close();
            session.close();
            driver.close();        
            return "Success";
        } catch(IOException | Neo4jException ioe) {
            ioe.printStackTrace();
            return "IO Error";
        }
    }    

    /**
     * importing *.xls* Excel file workbook or 97 spreadsheet data into neo4j db (version with open connection add methods)
     * @param filename 
     * @return String import status
     */    
    public String importExcelDataOpenConnection(String filename) {
        String result;
        String extension = "";
        if (filename != null & filename !="" & filename.indexOf(".") != -1) {
            extension = filename.substring(filename.indexOf("."));
        }
        switch (extension) {
            case ".xls":    result = importXLSOpenConnection(filename);
                            break;
            case ".xlsx":   result = importXLSXOpenConnection(filename);
                            break;
            default:        result = "wrong filename structure!";
                            break;
        }
        return result;
    }      
    
    
    
}