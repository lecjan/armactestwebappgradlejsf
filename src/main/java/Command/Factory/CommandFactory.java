/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package Command.Factory;

/**
 *
 * @author Lech Jankowski
 */
public class CommandFactory {
 
	/**
	 *
	 * @param commandStr
	 * @return command
	 */
	public Command createCommand(String commandStr) 
    {
    	Command command = null;
		switch (commandStr) {
			case "ImportData"		:	command = new ImportData();
										break;
			case "DeleteData"		:	command = new DeleteData();
										break;                            
			case "FindShortestPath"		:	command = new FindShortestPath();
										break;    
			case "FindAllPaths"		:	command = new FindAllPaths();
										break;                               
			case "RenderAllConnections"	:	command = new RenderAllConnections();
										break;
			case "SearchAllNeighbours"         :	command = new SearchAllNeighbours();
										break;
                        case "gotoHomePage"             :	command = new GotoHomePage();
										break;
			case "notset"			:	command = new GotoHomePage();
										break;
			default				:	command = new GotoHomePage();
										break;
		}
    	return command;
    }     
}
