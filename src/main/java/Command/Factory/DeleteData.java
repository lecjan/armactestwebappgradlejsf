/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package Command.Factory;

import Messages.AppWarningMessageImport;
import Messages.AppWarningMessageSearch;
import Service.Neo4jService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Imports data from excel to neo4j graph database
 * @author Lech Jankowski
 */
public class DeleteData implements Command{

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardTo;
        String result;

        Neo4jService neo4js = new Neo4jService();
        result = neo4js.deleteData();
        if (result == "Success") {
            forwardTo = "/index.jsp";
            AppWarningMessageImport.setText("You have deleted data succesfully!"); 
            AppWarningMessageSearch.setText(""); 
        } else {
            forwardTo = "/index.jsp";
            AppWarningMessageImport.setText("Error while deleting data!"); 
            AppWarningMessageSearch.setText(""); 
        }
        return forwardTo;
    }
}
