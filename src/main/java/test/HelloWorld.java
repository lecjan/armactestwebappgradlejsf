package test;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

@ManagedBean
@SessionScoped

public class HelloWorld implements Serializable {
    private String message = "Hello World message!";

    public HelloWorld() {
        System.out.println("HelloWorld started!");
    }

    public String getMessage() {
        return this.message;
    }
}
