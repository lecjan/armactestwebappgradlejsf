/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package DTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Lech Jankowski
 */
public class TransitPathWeighted {
    
    private List<String> cities;
    private List<Double> transittime;
    private double totaltransittime;

    public TransitPathWeighted() {
        this.cities = new ArrayList<String>();
        this.transittime = new ArrayList<Double>();
        this.totaltransittime = 0.0;
    }

    public TransitPathWeighted(List<String> cities, List<Double> transittime, double totaltransittime) {
        this.cities = cities;
        this.transittime = transittime;
        this.totaltransittime = totaltransittime;
    }

    public List<String> getCities() {
        return cities;
    }

    public void setCities(List<String> cities) {
        this.cities = cities;
    }

    public List<Double> getTransittime() {
        return transittime;
    }

    public void setTransittime(List<Double> transittime) {
        this.transittime = transittime;
    }

    public double getTotaltransittime() {
        return totaltransittime;
    }

    public void setTotaltransittime(double totaltransittime) {
        this.totaltransittime = totaltransittime;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.cities);
        hash = 67 * hash + Objects.hashCode(this.transittime);
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.totaltransittime) ^ (Double.doubleToLongBits(this.totaltransittime) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TransitPathWeighted other = (TransitPathWeighted) obj;
        if (!Objects.equals(this.cities, other.cities)) {
            return false;
        }
        if (!Objects.equals(this.transittime, other.transittime)) {
            return false;
        }
        if (Double.doubleToLongBits(this.totaltransittime) != Double.doubleToLongBits(other.totaltransittime)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TransitPathWeighted{" + "cities=" + cities + ", transittime=" + transittime + ", totaltransittime=" + totaltransittime + '}';
    }
    
    public String toStringFormatted() {
        String sf = "";
        for (int i=0; i < cities.size(); i++) {
            sf = sf + cities.get(i);
            if (i < transittime.size()) sf = sf + " - " + transittime.get(i) + "h - ";
        }
        sf = sf + " (Total time: " + totaltransittime + "h}";
        return sf;
    }
    
    public void addCity(String city) {
        this.cities.add(city);
    }
    
    public void addTransitTime(double transittime) {
        this.transittime.add(transittime);
    }
    
    public boolean isEmpty() {
        return (this.cities.isEmpty() & this.transittime.isEmpty() & (this.totaltransittime == 0.0));
    }
    
    
}
