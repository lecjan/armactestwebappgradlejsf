/*
 * Copyright (C) 2017 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package Service;

import DAO.DAO_Neo4jGraphDB;
import DTO.CityList;
import DTO.TransitPathWeighted;
import DTO.TransitPathWeightedList;

public class Neo4jService {

    /**
     * Imports data from excel to neo4j graph database
     * @param filename
     * @return 
     */
    public String importData(String filename) {
        DAO_Neo4jGraphDB neo4jDAO = new DAO_Neo4jGraphDB();
        String result = neo4jDAO.importExcelDataOpenConnection(filename);
        return result;
    }
    
    public String deleteData() {
        String result = "Success";
        DAO_Neo4jGraphDB neo4jDAO = new DAO_Neo4jGraphDB();
        neo4jDAO.eraseDatabase();
        return result;
    }
    
    public TransitPathWeighted searchShortestPath(String source, String destination) {
        DAO_Neo4jGraphDB neo4jDAO = new DAO_Neo4jGraphDB();
        TransitPathWeighted result = neo4jDAO.getShortestPath(source, destination);
        return result;
    }    
    
    public TransitPathWeightedList searchAllPaths(String source, String destination) {
        DAO_Neo4jGraphDB neo4jDAO = new DAO_Neo4jGraphDB();
        TransitPathWeightedList result = neo4jDAO.getPathsAscendingByTime(source, destination);
        return result;
    }      
    
    public TransitPathWeightedList searchAllConnections() {
        DAO_Neo4jGraphDB neo4jDAO = new DAO_Neo4jGraphDB();
        TransitPathWeightedList result = neo4jDAO.getAllTransits();
        return result;
    }     
    
    public TransitPathWeightedList searchAllNeighbours(String source) {
        DAO_Neo4jGraphDB neo4jDAO = new DAO_Neo4jGraphDB();
        TransitPathWeightedList result = neo4jDAO.getAllNeighbours(source);
        return result;
    }     
    
    
}
