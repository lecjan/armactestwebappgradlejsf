<%-- 
    Document   : index
    Created on : 23-Apr-2017, 19:40:17
    Author     : Lech Jankowski
--%>

<%@page import="DTO.TransitPathWeighted"%>
<%@page import="DTO.TransitPathWeightedList"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="Messages.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">

        <title>Armac Systems Technical Test - Lech Jankowski</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">   
        
        <script>
            var geocoder;
            var map;
            var cities = [];
            
            // initialising google map
            function initialize() {
              geocoder = new google.maps.Geocoder();
              var mapOptions = {
                zoom: 4,
                center: {lat:53.3439,lng:23.0622} // centre of Europe
              }
              map = new google.maps.Map(document.getElementById('simpleMap'), mapOptions);
            }

            // plotting marker on the map with given location name
            function codeAddressPlot(a) {
              var address = a;
              var location;
              geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == 'OK') {
                  location = results[0].geometry.location;
                  //map.setCenter(location);
                  var marker = new google.maps.Marker({
                      map: map,
                      position: location,
                      title: address
                  });
                } else {
                  //alert('Geocode was not successful for the following reason: ' + status);
                }
              });
            }      
            
            // in test - retrieving geolocation of a named location
            function codeAddressLocation(a) {
              var address = a;
              var location;
              geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == 'OK') {
                  location = results[0].geometry.location;
                  //alert(location);
                  return location;
                } else {
                  //alert('Geocode was not successful for the following reason: ' + status);
                }
              });
             
            }              

            // plotting all markers from global array "cities"
            function plotMarkers() {
                  var arrayLength = cities.length;
                  for (var i = 0; i < arrayLength; i++) {
                      codeAddressPlot(cities[i]);
                  }              
            }
          
            // in test - plotting a polyline on the map
            function plotPolylines() {
                
                var flightPlanCoordinates = [
                  {lat: 37.772, lng: -122.214},
                  {lat: 21.291, lng: -157.821},
                  {lat: -18.142, lng: 178.431},
                  {lat: -27.467, lng: 153.027}
                ];
                var path = [];
                path.push(new google.maps.LatLng(codeAddressLocation('Gdansk')));
                path.push(new google.maps.LatLng(codeAddressLocation('Zakopane')));

                path.push(new google.maps.LatLng(37.77,-122.21));
                path.push(new google.maps.LatLng(21.29,-157.82));
        
                alert(path);
                
                var flightPath = new google.maps.Polyline({
                  path: path,
                  geodesic: true,
                  strokeColor: '#FF0000',
                  strokeOpacity: 1.0,
                  strokeWeight: 2
                });

                flightPath.setMap(map);          
            }
         
        </script>        
        
    </head>
    <body onload="initialize();plotMarkers();"> <!-- map and markers are drawn at each page load -->
        
        <% TransitPathWeightedList tpwl = (TransitPathWeightedList) (request.getSession().getAttribute("transits")); 
        String city;
        if (tpwl != null) {
            for (int i=0; i < tpwl.getPaths().size(); i++) {
                TransitPathWeighted tpw = tpwl.getPaths().get(i);
                for (int j=0; j < tpw.getCities().size(); j++) {
                    city = tpw.getCities().get(j);
        %>        
                    <script>
                        cities.push('<%=city%>');
                    </script>
        <%
                }
            }
        }
        %>         
        
        
        
        <div class="container">
          <h1>Armac Systems Technical Test</h1>
          <p>by Lech Jankowski</p> 


            <!-- Search and Notes areas ------------------------------------------>

            <div class="panel-group">
                <div class="panel panel-success">

                    <div class="panel-heading">Import Data</div>

                    <div class="panel-body">

                        <div class="col-xs-6">
                            <label for="import">Enter filename of Excel document to load data to database:</label>
                            <h6>Note! Excel Sheet column structure accepted: source,destination,time</h6>
                            <form method="post" action="ActionServlet" > 
                                <div class="input-group">
                                  <input name="filename_text" type="text" size="40" class="form-control" id="import" >
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Start Import</button>
                                  </span>
                                </div>
                                <input type="hidden" name="action" value="ImportData" id="submit_action"/>
                            </form>
                        </div>
                        <div class="col-xs-6">
                            <form method="post" action="ActionServlet" > 
                                <button class="btn btn-default" type="submit">Erase Neo4j DB</button>
                                <input type="hidden" name="action" value="DeleteData" id="submit_action"/>
                            </form>
                        </div>                        
                        <div class="col-xs-6">
                            <% out.println(AppWarningMessageImport.getText()); %>
                        </div>                    
                    </div>
                </div>

                <div class="panel panel-info">

                    <div class="panel-heading">Search</div>
                    <div class="panel-body">
                        <div class="col-xs-3">
                            <form method="post" action="ActionServlet" > 
                                <div class="form-group">
                                    <label for="src">Source City:</label>
                                    <h6>City names are case sensitive</h6>
                                    <input name="search_source" type="text" size="20" class="form-control" id="src">
                                </div>
                                <div class="form-group">
                                    <label for="dst">Destination City:</label>
                                    <input name="search_destination" type="text" size="20" class="form-control" id="dst">
                                </div>                              
                                <button class="btn btn-default" type="submit" onclick="plotMarkers();">Find Shortest Path</button>
                                <input type="hidden" name="action" value="FindShortestPath" id="submit_action"/>
                            </form>
                        </div>     
                        <div class="col-xs-3">
                            <form method="post" action="ActionServlet" > 
                                <div class="form-group">
                                    <label for="src">Source City:</label>
                                    <h6>City names are case sensitive</h6>
                                    <input name="search_source" type="text" size="20" class="form-control" id="src">
                                </div>
                                <div class="form-group">
                                    <label for="dst">Destination City:</label>
                                    <input name="search_destination" type="text" size="20" class="form-control" id="dst">
                                </div>                              
                                <button class="btn btn-default" type="submit" onclick="plotMarkers();">Find All Possible Paths</button>
                                <input type="hidden" name="action" value="FindAllPaths" id="submit_action"/>
                            </form>
                        </div>  
                        <div class="col-xs-3">
                            <form method="post" action="ActionServlet" > 
                                <div class="form-group">
                                    <label for="src">Source City:</label>
                                    <h6>City names are case sensitive</h6>
                                    <input name="search_source" type="text" size="20" class="form-control" id="src">
                                </div>
                                <button class="btn btn-default" type="submit" onclick="plotMarkers();">Find All Neighbours</button>
                                <input type="hidden" name="action" value="SearchAllNeighbours" id="submit_action"/>
                            </form>
                        </div>                         
                        <div class="col-xs-3">
                            <form method="post" action="ActionServlet" > 
                                <button class="btn btn-default" type="submit" onclick="plotMarkers();">Display All Connections</button>
                                <input type="hidden" name="action" value="RenderAllConnections" id="submit_action"/>
                            </form>
                        </div>  
                    </div>
                </div>
            </div>        

            <!-- Results areas ---------------------------------------------------->

            <div class="panel panel-default">
                <div class="panel-heading">Results</div>
                <div class="panel-body">
                    <% out.println(AppWarningMessageSearch.getText()); %>
                    </br>
                    <% out.println(SearchResultMessage.getText()); %>
                </div>
            </div>        

            <!-- Map area --------------------------------------------------------->

            <!-- Google maps goes here -->
            <div class="panel panel-default">
                <div class="panel-heading">Tour Map</div>
                <div class="panel-body">
                        <div id="simpleMap" style="height:500px;"></div>
                </div>
            </div>
            <!-- end google maps -->          

            <!-- Bootstrap Core JavaScript -->
            <script src="js/bootstrap.min.js"></script>

        </div>

        <!-- Google Maps -->
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj20qCYRGPtOtNdrjAstzbt3N8-0TOLqg&callback=initMap" type="text/javascript"></script>     
        
    </body>
</html>
